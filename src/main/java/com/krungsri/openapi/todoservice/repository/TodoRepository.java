package com.krungsri.openapi.todoservice.repository;

import com.krungsri.openapi.todoservice.domain.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepository extends JpaRepository<Todo, Long> {
}
