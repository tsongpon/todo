package com.krungsri.openapi.todoservice.controller;

import com.krungsri.openapi.todoservice.service.PingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {

    @Autowired
    private PingService pingService;

    @GetMapping("/ping")
    public String ping() {
        return pingService.pongMessage();
    }
}
