package com.krungsri.openapi.todoservice.controller;

import com.krungsri.openapi.todoservice.domain.Todo;
import com.krungsri.openapi.todoservice.service.TodoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TodoController {

    Logger logger = LoggerFactory.getLogger(TodoController.class);

    @Autowired
    private TodoService todoService;

    @GetMapping("/todos/{id}")
    public Todo getById(@PathVariable Long id) {
        var name = "tum";
        logger.info("Getting todo for id {}, name {}", id, name);
        return todoService.getById(id);
    }

    @PostMapping("/todos")
    public ResponseEntity<Todo> create(@RequestBody Todo todo) {
        logger.info("Saving new todo name {}", todo.getName());
        todoService.saveTodo(todo);
        return ResponseEntity.status(201).build();
    }

    @GetMapping("todos")
    public List<Todo> listAll() {
        return todoService.listTodo();
    }
}
