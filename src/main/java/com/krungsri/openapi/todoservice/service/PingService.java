package com.krungsri.openapi.todoservice.service;

import org.springframework.stereotype.Service;

@Service
public class PingService {
    public String pongMessage() {
        return "pong pong";
    }
}
