package com.krungsri.openapi.todoservice.service;

import com.krungsri.openapi.todoservice.domain.Todo;
import com.krungsri.openapi.todoservice.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    @Autowired
    private TodoRepository todoRepository;

    public Todo getById(Long id) {
        var todo = new Todo();
        todo.setId(1L);
        todo.setName("Learn Java");
        todo.setDescription("Learn java for backend task");
        return todo;
    }

    public void saveTodo(Todo todo) {
        todoRepository.save(todo);
    }

    public List<Todo> listTodo() {
        return todoRepository.findAll();
    }
}
